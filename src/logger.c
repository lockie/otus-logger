#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <execinfo.h>

#include "sqlite3.h"

#include "logger.h"


static sqlite3* db = NULL;

static const char* create_tables_sql =
    "CREATE TABLE IF NOT EXISTS messages ("
    "  id INTEGER PRIMARY KEY,"
    "  reported INTEGER NOT NULL,"
    "  severity INTEGER NOT NULL,"
    "  source TEXT NOT NULL,"
    "  line INTEGER NOT NULL,"
    "  message TEXT NOT NULL"
    ");"
    "CREATE TABLE IF NOT EXISTS stack_crawls ("
    "  id INTEGER PRIMARY KEY,"
    "  message_id INTEGER NOT NULL,"
    "  address TEXT NOT NULL,"
    "  FOREIGN KEY(message_id) REFERENCES messages(id)"
    ");";

static const char insert_message_sql[] =
    "INSERT INTO messages (reported, severity, source, line, message) "
    "VALUES (datetime('now'), ?1, ?2, ?3, ?4) RETURNING id;";

static const char insert_stack_crawl_sql[] =
    "INSERT INTO stack_crawls (message_id, address) VALUES (?1, ?2);";

static sqlite3_stmt* insert_message_stmt = NULL;

static sqlite3_stmt* insert_stack_crawl_stmt = NULL;


bool init_log(const char* logfile)
{
    int rc = sqlite3_initialize();
    if(rc != SQLITE_OK)
    {
        fprintf(stderr, "Failed to initialize SQLite: %s\n",
                sqlite3_errstr(rc));
        return false;
    }

    if(sqlite3_open(logfile, &db) != SQLITE_OK)
    {
        fprintf(stderr, "Failed to open log file '%s': %s\n",
                logfile, sqlite3_errmsg(db));
        return false;
    }

    if(sqlite3_exec(db, create_tables_sql, NULL, NULL, NULL) != SQLITE_OK)
    {
        fprintf(stderr, "Failed to create logging tables: %s\n",
                sqlite3_errmsg(db));
        goto fail;
    }

    if(sqlite3_prepare_v3(
           db, insert_message_sql, sizeof(insert_message_sql),
           SQLITE_PREPARE_PERSISTENT, &insert_message_stmt, NULL) != SQLITE_OK)
    {
        fprintf(stderr, "Failed to prepare message statement: %s\n",
                sqlite3_errmsg(db));
        goto fail;
    }

    if(sqlite3_prepare_v3(
           db, insert_stack_crawl_sql,
           sizeof(insert_stack_crawl_sql), SQLITE_PREPARE_PERSISTENT,
           &insert_stack_crawl_stmt, NULL) != SQLITE_OK)
    {
        fprintf(stderr, "Failed to prepare stack crawl statement: %s\n",
                sqlite3_errmsg(db));
        goto fail;
    }

    return true;

fail:
    sqlite3_close(db);
    db = NULL;
    return false;
}

static const char* severities[] = {"DEBUG", "INFO", "WARNING", "ERROR"};

void do_log(const char* func, const char* file, long line,
            severity_t severity, const char* message, ...)
{
    if(!db)
    {
        fputs("Library wasn't properly initialized", stderr);
        return;
    }

    /* insert message */
    if(sqlite3_bind_int(insert_message_stmt, 1, severity) != SQLITE_OK)
    {
        fprintf(stderr, "Failed to bind severity: %s\n", sqlite3_errmsg(db));
        return;
    }

    if(sqlite3_bind_text(insert_message_stmt, 2, file, -1, SQLITE_STATIC)
       != SQLITE_OK)
    {
        fprintf(stderr, "Failed to bind file: %s\n", sqlite3_errmsg(db));
        return;
    }

    if(sqlite3_bind_int64(insert_message_stmt, 3, line) != SQLITE_OK)
    {
        fprintf(stderr, "Failed to bind line: %s\n", sqlite3_errmsg(db));
        return;
    }

    va_list vl;
    va_start(vl, message);
    int buffer_len = vsnprintf(NULL, 0, message, vl);
    va_end(vl);

    char* buffer = malloc(buffer_len + 1);
    if(!buffer)
        return;

    va_start(vl, message);
    vsnprintf(buffer, buffer_len + 1, message, vl);
    va_end(vl);

    if(sqlite3_bind_text(insert_message_stmt, 4, buffer, buffer_len + 1,
                         SQLITE_STATIC) != SQLITE_OK)
    {
        fprintf(stderr, "Failed to bind message: %s\n", sqlite3_errmsg(db));
        free(buffer);
        return;
    }

    if(sqlite3_step(insert_message_stmt) != SQLITE_ROW)
    {
        fprintf(stderr, "Failed to insert message: %s\n", sqlite3_errmsg(db));
        free(buffer);
        return;
    }

    sqlite3_int64 message_id = sqlite3_column_int64(insert_message_stmt, 0);

    sqlite3_reset(insert_message_stmt);
    sqlite3_clear_bindings(insert_message_stmt);

    free(buffer);

    /* insert stack crawls */
    if(sqlite3_bind_int64(insert_stack_crawl_stmt, 1, message_id) != SQLITE_OK)
    {
        fprintf(stderr, "Failed to bind message id: %s\n", sqlite3_errmsg(db));
        return;
    }

    int bt_buffer_len = 4;
    int nptrs = 0;
    void** bt_buffer = NULL;
    do
    {
        bt_buffer_len *= 2;
        bt_buffer = realloc(bt_buffer, bt_buffer_len * sizeof(void*));
        if(!bt_buffer)
            return;
        nptrs = backtrace(bt_buffer, bt_buffer_len);
    } while(nptrs == bt_buffer_len);

    char** crawls = backtrace_symbols(bt_buffer, nptrs);
    if(!crawls)
    {
        free(bt_buffer);
        return;
    }

    for(int i = 1; i < nptrs; i++)
    {
        if(sqlite3_bind_text(insert_stack_crawl_stmt, 2, crawls[i], -1,
                             SQLITE_STATIC) != SQLITE_OK)
        {
            fprintf(stderr, "Failed to bind crawl: %s\n", sqlite3_errmsg(db));
            goto crawl_cleanup;
        }

        if(sqlite3_step(insert_stack_crawl_stmt) != SQLITE_DONE)
        {
            fprintf(stderr, "Failed to insert crawl: %s\n", sqlite3_errmsg(db));
            goto crawl_cleanup;
        }

        sqlite3_reset(insert_stack_crawl_stmt);
    }

crawl_cleanup:
    sqlite3_reset(insert_stack_crawl_stmt);
    sqlite3_clear_bindings(insert_stack_crawl_stmt);

    free(crawls);
    free(bt_buffer);
}

void fini_log()
{
    sqlite3_finalize(insert_stack_crawl_stmt);
    sqlite3_finalize(insert_message_stmt);
    sqlite3_close(db);
    sqlite3_shutdown();
}

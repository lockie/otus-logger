
#include "logger.h"


void testf2(int i)
{
    static const char* blarg = "frobnication has failed";
    LOG_ERROR("unable to continue: %d, %s", i, blarg);
}

void testf1()
{
    testf2(42);
}

int main(int argc, char** argv)
{
    if(!init_log("log.sqlite"))
        return 1;

    LOG_DEBUG("main started with %d args from %s", argc, argv[0]);

    LOG_INFO("everything is ok");

    LOG_WARNING("oops, something went wrong");

    testf1();

    fini_log();

    return 0;
}

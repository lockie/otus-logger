
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR})

set(SOURCES "example.c")

include_directories("../include")

add_executable(Example ${SOURCES})
target_link_libraries(Example logger)
# https://stackoverflow.com/a/60768340/1336774
target_link_options(Example PUBLIC "-export-dynamic")


#ifndef _LOGGER_H_
#define _LOGGER_H_

#include <stdbool.h>


#ifdef __cplusplus
extern "C" {
#endif

typedef enum {
    DEBUG = 0,
    INFO,
    WARNING,
    ERROR
} severity_t;


#if defined(__GNUC__) || defined(__clang__)
#define PRINTFLIKE(n, m) __attribute__((format(printf, n, m)))
#else
#define PRINTFLIKE(n, m)
#endif // __GNUC__

extern bool init_log(const char* logfile);

extern void do_log(const char* func, const char* file, long line,
                   severity_t severity, const char* message,
                   ...) PRINTFLIKE(5, 6);

extern void fini_log();

#define LOG_DEBUG(...) \
    do_log(__func__, __FILE__, __LINE__, DEBUG, __VA_ARGS__)
#define LOG_INFO(...) \
    do_log(__func__, __FILE__, __LINE__, INFO, __VA_ARGS__)
#define LOG_WARNING(...) \
    do_log(__func__, __FILE__, __LINE__, WARNING, __VA_ARGS__)
#define LOG_ERROR(...) \
    do_log(__func__, __FILE__, __LINE__, ERROR, __VA_ARGS__)

#ifdef __cplusplus
}
#endif

#endif  // _LOGGER_H_
